#pragma once

/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             conker/test.h
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <conker/test/lib.h>
#include <stddef.h>
#include <stdio.h>

#ifndef DOXYGEN_SHOULD_SKIP_THIS
CONKER_TEST_EXTERN_C_BEGIN
#endif

/**
 * @file
 * @brief @c conker-test is a unit test library for C programming
 * language.
 *
 * The library consists of a number of functions, macroses, and
 * typedefs used to define tests functions, make assertions, run
 * tests, collect test results, print reports, etc.
 *
 * Most of the users need only few items from the library:
 *
 * @li #ctAssert
 * @li #ctAssertFailure
 * @li #ctAssertSuccess
 * @li #ctTestMain
 * @li #ctDefineTest
 *
 * The following sections will guide you step by step how to define
 * tests, run them, etc.
 *
 * ## Define tests
 *
 * Every test function should have the following signature:
 *
 * @code{.c}
 * ctAssertionFailure *testFunction();
 * @endcode
 *
 * That is a test function is a function that takes no parameters and
 * returns a pointer to #ctAssertionFailure object.
 *
 * The #ctAssertionFailure object is used to capture
 * various information about assertion failure. However, you shouldn't
 * use it directly, because there are a few convenient macroses
 * defined for you:
 *
 * @li #ctAssert
 * @li #ctAssertFailure
 * @li #ctAssertSuccess
 *
 * Let's define a simple test function:
 *
 * @code{.c}
 * #include <conker/test.h>
 *
 * ctAssertionFailure *
 * testTwoPlusTwoIsFour()
 * {
 *         ctAssert(2 + 2 == 4, "Two plus two must be four");
 *         ctAssertSuccess();
 * }
 *
 * int
 * main(int argc, char **argv)
 * {
 * }
 * @endcode
 *
 * The @c testTwoPlusTwoIsFour function uses #ctAssert macro to check
 * if the given condition is @c true and fail if it is @c false.
 *
 * The function uses #ctAssertSuccess macro to indicate that the test
 * has completed successfuly. Always put this macro at the end of your
 * test function or it will not compile.
 *
 * Let's add one more test.
 *
 * @code{.c}
 * #include <conker/test.h>
 *
 * ctAssertionFailure *
 * testTwoPlusTwoIsFour()
 * {
 *         ctAssert(2 + 2 == 4, "Two plus two must be four");
 *         ctAssertSuccess();
 * }
 *
 * ctAssertionFailure *
 * testFiveMinusTwoIsOne()
 * {
 *         // This one should fail
 *         ctAssert(5 - 2 == 1, "Five minus two must be one");
 *         ctAssertSuccess();
 * }
 *
 * int
 * main(int argc, char **argv)
 * {
 * }
 * @endcode
 *
 * Obviously, @c testFiveMinusTwoIsOne should fail.
 *
 * ## Collect tests
 *
 * The next step is to collect all tests so that we can put them
 * somewhere to run.
 *
 * @code{.c}
 * #include <conker/test.h>
 *
 * ctAssertionFailure *
 * testTwoPlusTwoIsFour()
 * {
 *         ctAssert(2 + 2 == 4, "Two plus two must be four");
 *         ctAssertSuccess();
 * }
 *
 * ctAssertionFailure *
 * testFiveMinusTwoIsOne()
 * {
 *         // This one should fail
 *         ctAssert(5 - 2 == 1, "Five minus two must be one");
 *         ctAssertSuccess();
 * }
 *
 * int
 * main(int argc, char **argv)
 * {
 *         static ctTestDefinition tests[] = {
 *                 ctDefineTest(testTwoPlusTwoIsFour),
 *                 ctDefineTest(testFiveMinusTwoIsOne),
 *         };
 *
 *         size_t testsCount = sizeof(tests) / sizeof(ctTestDefinition);
 * }
 * @endcode
 *
 * Here we use #ctDefineTest macro to transform a test function into a
 * test definition.
 *
 * We collect all tests into a static array and then calculate the
 * number of its elements for future use.
 *
 * ## Run tests
 *
 * @code{.c}
 * #include <conker/test.h>
 * #include <stdio.h>
 *
 * ctAssertionFailure *
 * testTwoPlusTwoIsFour()
 * {
 *         ctAssert(2 + 2 == 4, "Two plus two must be four");
 *         ctAssertSuccess();
 * }
 *
 * ctAssertionFailure *
 * testFiveMinusTwoIsOne()
 * {
 *         // This one should fail
 *         ctAssert(5 - 2 == 1, "Five minus two must be one");
 *         ctAssertSuccess();
 * }
 *
 * int
 * main(int argc, char **argv)
 * {
 *         static ctTestDefinition tests[] = {
 *                 ctDefineTest(testTwoPlusTwoIsFour),
 *                 ctDefineTest(testFiveMinusTwoIsOne),
 *         };
 *
 *         size_t testsCount = sizeof(tests) / sizeof(ctTestDefinition);
 *
 *         return ctTestMain(argc, argv, stdout, tests, testsCount);
 * }
 * @endcode
 *
 * There are a few changes here. First, we include the @c <stdio.h>
 * file so that we can refer to @c stdout. Second, we added call to
 * #ctTestMain function.
 *
 * And that's it. Done.
 *
 * Now you can compile the program and run it without arguments. It
 * will run all tests and print a report with the list of succeeded
 * and failed tests. Failed tests will include information about the
 * test name, path to the source file, the function name, and the
 * source code line.
 *
 * For example this is the output for the program we implemented:
 *
 * @code{.unparsed}
 * [SUCCESS] testTwoPlusTwoIsFour.
 * [FAILURE] testFiveMinusTwoIsOne. Five minus two must be one. [(5 - 2 == 1) is false] at
 * /home/vault-boy/Documents/Projects/conker/subprojects/conker-draw/test/conker/draw/test_main.c:testFiveMinusTwoIsOne:L25.
 *
 * Total: 2
 * Succeeded: 1
 * Failed: 1
 * @endcode
 *
 * You can run the program with a single command line argument - the
 * name of the test to run. In this case it will run only that test:
 *
 * @code{.unparsed}
 * $ ./build/test-examples testTwoPlusTwoIsFour.
 * [SUCCESS] testTwoPlusTwoIsFour.
 * @endcode
 *
 * The test that fails:
 *
 * @code{.unparsed}
 * $ ./build/test-examples testFiveMinusTwoIsOne.
 * [FAILURE] testFiveMinusTwoIsOne. Five minus two must be one. [(5 - 2 == 1) is false] at
 * /home/vault-boy/Documents/Projects/conker/subprojects/conker-draw/test/conker/draw/test_main.c:testFiveMinusTwoIsOne:L25.
 * @endcode
 *
 * A wrong test name:
 *
 * @code{.unparsed}
 * $ ./build/test-examples testDoesNotExist
 * [ERROR] Cannot find test 'testDoesNotExist'.
 * @endcode
 *
 * The #ctTestMain function returns @c EXIT_SUCCESS if all tests have
 * completed successfuly and @c EXIT_FAILURE otherwise.
 *
 * ## CTest
 *
 * You can easily integrate conker-test library with CTest which is a
 * part of CMake. For example:
 *
 * @code{.cmake}
 * enable_testing()
 * add_executable(test-example test/test_main.c)
 * target_link_libraries(test-example conker-test)
 *
 * add_test(
 *   NAME    testTwoPlusTwoIsFour
 *   COMMAND test-example "testTwoPlusTwoIsFour")
 * add_test(
 *   NAME    testFiveMinusTwoIsOne
 *   COMMAND test-example "testFiveMinusTwoIsOne")
 * @endcode
 *
 * And then run @c ctest inside your build directory. If a test fails,
 * you can analyse the file @c Testing/Temporary/LastTest.log under
 * your CMake build directory. Another option is to run @c ctest with
 * @c --verbose flag.
 */

/**
 * @brief Describes an assertion failure.
 *
 * Use the following macroses inside a test function (i.e. function of
 * the #ctTestFn type) to create assertion failures, unless you want
 * to implement custom test runners/reports:
 *
 * @li #ctAssert
 * @li #ctAssertFailure
 * @li #ctAssertSuccess
 *
 * For example:
 *
 * @code{.c}
 * ctAssertionFailure *
 * testSomeNonsense()
 * {
 *         ctAssert(2 > 3, "Two cannot be greater than three");
 *         ctAssertSuccess();
 * }
 *
 * ctAssertionFailure *
 * testAnotherNonsense()
 * {
 *         ctAssertFailure("Not implemented");
 * }
 * @endcode
 *
 * @sa ctAssertionFailureCreate
 * @sa ctAssertionFailureDestroy
 * @sa ctTestRunResult
 */
typedef struct ctAssertionFailure {
	/**
	 * @brief An error message. It @b must be a static constant
	 * string.
	 */
	char const *message;
	/**
	 * @brief An expession that caused an error. It @b must be a
	 * static constant string.
	 */
	char const *expession;
	/**
	 * @brief A file that contains the code that caused an
	 * error. It @b must be a static constant string.
	 */
	char const *file;
	/**
	 * @brief A function that contains the code that caused an
	 * error. It @b must be a static constant string.
	 */
	char const *function;
	/**
	 * @brief A source code line number that caused an error.
	 */
	size_t line;
} ctAssertionFailure;

/**
 * @brief A type of a function that returns an assertion failure. A
 * function returns @c NULL to indicate successful completion,
 * i.e. absence of a failure.
 *
 * For example:
 *
 * @code{.c}
 * ctAssertionFailure *
 * testSomeNonsense()
 * {
 *         ctAssert(2 > 3, "Two cannot be greater than three");
 *         ctAssertSuccess();
 * }
 * @endcode
 *
 * @sa ctTestDefinition
 * @sa ctAssert
 * @sa ctAssertFailure
 * @sa ctAssertSuccess
 * @sa ctTestRunResult
 */
typedef ctAssertionFailure *(*ctTestFn)();

/**
 * @brief Describes a test.
 *
 * There is no need to use @c ctTestDefinition directly, unless you
 * want to implement a custom test runner. Use #ctDefineTest instead:
 *
 * @code{.c}
 * static ctTestDefinition tests[] = {
 *         ctDefineTest(testPaletteCreate),
 *         ctDefineTest(testPaletteDestroy),
 * };
 * @endcode
 *
 * @sa ctTestFn
 * @sa ctTestRunTests
 * @sa ctDefineTest
 * @sa ctTestMain
 */
typedef struct ctTestDefinition {
	/**
	 * @brief A name of the test. It @b must be a static constant
	 * string.
	 */
	char const *name;
	/**
	 * A function that executes the test.
	 */
	ctTestFn fn;
} ctTestDefinition;

/**
 * @brief Result of a single test run.
 *
 * @sa ctCreateTestResult
 * @sa ctDestroyTestResult
 * @sa ctTestRunResults
 * @sa test_run
 * @sa ctTestRunTests
 */
typedef struct ctTestRunResult {
	/**
	 * @brief A name of the test. It @b must be a static constant string.
	 */
	char const *name;
	/**
	 * @brief An object that describes the cause of the
	 * failure. The run has completed successfuly if the field is @c NULL.
	 */
	ctAssertionFailure *failure;
} ctTestRunResult;

/**
 * @brief A collection of objects of #ctTestRunResult type.
 *
 * @sa ctCreateTestRunResults
 * @sa ctDestroyTestRunResults
 * @sa ctTestRunReport
 */
typedef struct ctTestRunResults {
	/**
	 * @brief A an array of pointers to objects of
	 * #ctTestRunResult type. The struct owns the array and
	 * its elements.
	 */
	ctTestRunResult **results;
	/**
	 * @brief A number of items in the @c results array.
	 */
	size_t count;
} ctTestRunResults;

/**
 * @brief Provides access to a collections of failed and successful
 * tests.
 *
 * @sa ctCreateTestRunReport
 * @sa ctDestroyTestRunReport
 * @sa ctPrintTestRunReport
 * @sa ctTestRunTests
 * @sa ctTestRunResults
 * @sa ctTestMain
 */
typedef struct ctTestRunReport {
	/**
	 * @brief An array of all test runs. The struct owns the
	 * array and its elements.
	 */
	ctTestRunResults *results;
	/**
	 * @brief An array of successful test runs. The struct owns
	 * the array. The array elements are pointers to @c results
	 * array.
	 */
	ctTestRunResult **successfulTests;
	/**
	 * @brief An array of failed test runs. The struct owns
	 * the array. The array elements are pointers to @c results
	 * array.
	 */
	ctTestRunResult **failedTests;
	/**
	 * @brief The number of elements in @c successfulTests.
	 */
	size_t successfulTestsCount;
	/**
	 * @brief The number of elements in @c failedTests.
	 */
	size_t failedTestsCount;
} ctTestRunReport;

/**
 * @brief Creates an instance of #ctAssertionFailure type and returns
 * a pointer to it.
 *
 * @param[in] message    An error message. It @b must be a static
 *                       constant string.
 * @param[in] expression An expession that caused an error. It @b must
 *                       be a static constant string.
 * @param[in] file       A file that contains the code that caused an
 *                       error. It @b must be a static constant string.
 * @param[in] function   A function that contains the code that caused an
 *                       error. It @b must be a static constant string.
 * @param[in] line       A source code line number that caused an error.
 *
 * @return A pointer to an instance of the #ctAssertionFailure.
 *
 * @remarks Use #ctDestroyAssertionFailure to destroy the instance.
 */
CONKER_TEST_EXPORT
ctAssertionFailure *
ctCreateAssertionFailure(
	char const *message,
	char const *expression,
	char const *file,
	char const *function,
	size_t      line);

/**
 * @brief Destroys an instance of #ctAssertionFailure type.
 *
 * @param[in] failure  A pointer to the #ctAssertionFailure instance
 *                     to destroy.
 *
 * @remarks Use #ctCreateAssertionFailure to create an instance.
 */
CONKER_TEST_EXPORT
void
ctDestroyAssertionFailure(ctAssertionFailure *failure);

/**
 * @brief Creates an instance of the #ctTestRunResult and
 * returns a pointer to it.
 *
 * @param[in] name    A name of the test. It @b must be a static constant
 *                    string.
 * @param[in] failure An object that describes the cause of the failure. The
 *                    run has completed successfuly if the @c failure
 *                    is @c NULL.
 *
 * @return A pointer to the instance of the #ctTestRunResult.
 *
 * @remarks Use #ctDestroyTestResult to destroy the instance.
 */
CONKER_TEST_EXPORT
ctTestRunResult *
ctCreateTestResult(char const *name, ctAssertionFailure *failure);

/**
 * @brief Destroys an instance of #ctTestRunResult type.
 *
 * @param[in] result  A pointer to the #ctTestRunResult instance
 *                    to destroy.
 *
 * @remarks Use #ctCreateTestResult to create an instance.
 */
CONKER_TEST_EXPORT
void
ctDestroyTestResult(ctTestRunResult *result);

/**
 * @brief Creates an instance of the #ctTestRunResults and
 * returns a pointer to it.
 *
 * @param[in] count A number of the test runs.
 *
 * @return A pointer to the instance of the #ctTestRunResults.
 *
 * @remarks Use #ctDestroyTestRunResults to destroy the instance.
 */
CONKER_TEST_EXPORT
ctTestRunResults *
ctCreateTestRunResults(size_t count);

/**
 * @brief Destroys an instance of #ctTestRunResults type.
 *
 * @param[in] results  A pointer to the #ctTestRunResults instance
 *                     to destroy.
 *
 * @remarks Use #ctCreateTestRunResults to create an instance.
 */
CONKER_TEST_EXPORT
void
ctDestroyTestRunResults(ctTestRunResults *results);

/**
 * @brief Creates an instance of the #ctTestRunReport and
 * returns a pointer to it.
 *
 * @param[in] results A collection of test run results.
 *
 * @return A pointer to the instance of the #ctTestRunReport.
 *
 * @remarks Use #ctDestroyTestRunReport to destroy the instance.
 *
 * @sa ctTestRunResults
 * @sa ctPrintTestRunReport
 * @sa ctTestRunTests
 */
CONKER_TEST_EXPORT
ctTestRunReport *
ctCreateTestRunReport(ctTestRunResults *results);

/**
 * @brief Destroys an instance of #ctTestRunReport type.
 *
 * @param[in] report  A pointer to the #ctTestRunReport instance
 *                    to destroy.
 *
 * @remarks Use #ctCreateTestRunReport to create an instance.
 */
CONKER_TEST_EXPORT
void
ctDestroyTestRunReport(ctTestRunReport *report);

/**
 * @brief Prints the given test run report into the given file.
 *
 * @param[in] stream The file the function should print the report to.
 * @param[in] report The report to print to the given file.
 *
 * @sa ctCreateTestRunReport
 */
CONKER_TEST_EXPORT
void
ctPrintTestRunReport(FILE *stream, ctTestRunReport *report);

/**
 * @brief Runs the given test definitions and returns an array of the resutls.
 *
 * @param[in] tests  An array of test definitions to run.
 * @param[in] count  The number of elements in @c tests array.
 *
 * @return An instance of #ctTestRunResults that captured all test runs.
 *
 * @sa ctTestRunReport
 * @sa ctCreateTestRunReport
 * @sa ctPrintTestRunReport
 */
CONKER_TEST_EXPORT
ctTestRunResults *
ctTestRunTests(ctTestDefinition *tests, size_t count);

/**
 * @brief Convenient function that parses command line arguments, runs
 * the given tests, prints reports, and returns the corresponding
 * status code.
 *
 * @param[in] argc        A number of command line arguments.
 * @param[in] argv        Command line arguments
 * @param[in] stream      A stream to print test run report to.
 * @param[in] tests       An array of test definitions to run.
 * @param[in] testsCount A number of elements in the @c tests array.
 *
 * @return @c EXIT_SUCCESS if all tests have completed successfully
 * and @c EXIT_FAILURE otherwise.
 *
 * This is the most convenient way to use the test library. It will
 * handle everything for you. All you have to do is define tests,
 * collect them into an array and call the #ctTestMain inside
 * your @c main C function.
 *
 * For example:
 *
 * @code{.c}
 * #include <conker/test.h>
 * #include <stdio.h>
 *
 * static
 * ctAssertionFailure *
 * testSomeNonsense()
 * {
 *         ctAssert(2 < 3, "Two cannot be greater than three");
 *         ctAssertSuccess();
 * }
 *
 * static
 * ctAssertionFailure *
 * testAnotherNonsense()
 * {
 *         ctAssert(2 > 3, "Two cannot be greater than three");
 *         ctAssertSuccess();
 * }
 *
 * int
 * main(int argc, char **argv)
 * {
 *         static ctTestDefinition tests[] = {
 *                 ctDefineTest(testSomeNonsense),
 *                 ctDefineTest(testAnotherNonsense),
 *         };
 *
 *         size_t testsCount = sizeof(tests) / sizeof(ctTestDefinition);
 *
 *         return ctTestMain(argc, argv, stdout, tests, testsCount);
 * }
 * @endcode
 *
 * @sa ctDefineTest
 * @sa ctAssert
 * @sa ctAssertFailure
 * @sa ctAssertSuccess
 * @sa ctTestFn
 */
CONKER_TEST_EXPORT
int
ctTestMain(
	int               argc,
	char **           argv,
	FILE *            stream,
	ctTestDefinition *tests,
	size_t            testsCount);

/**
 * @brief Checks whether given expression evaluates to @c true and
 * returns a ctAssertionFailure if it is not.
 *
 * @param[in] expression An expression to check.
 * @param[in] message    An error message.
 *
 * @remarks Use it only inside a function of #ctTestFn type.
 *
 * @sa ctAssertFailure
 * @sa ctAssertSuccess
 */
#define ctAssert(expression, message)                                          \
	do {                                                                   \
		if (!(expression))                                             \
			return ctCreateAssertionFailure(                       \
				"" message "",                                 \
				#expression,                                   \
				__FILE__,                                      \
				__func__,                                      \
				__LINE__);                                     \
	} while (0)

/**
 * @brief Returns a ctAssertionFailure with the given error message.
 *
 * @param[in] message    An error message.
 *
 * @remarks Use it only inside a function of #ctTestFn type.
 *
 * @sa ctAssert
 * @sa ctAssertSuccess
 */
#define ctAssertFailure(message)                                               \
	return ctCreateAssertionFailure(                                       \
		"" message "", NULL, __FILE__, __func__, __LINE__)

/**
 * @brief Returns an indicator that there is no any assertion failure.
 *
 * @remarks Use it only inside a function of #ctTestFn type.
 * @remarks Call it at the end of any test function.
 *
 * @sa ctAssert
 * @sa ctAssertFailure
 */
#define ctAssertSuccess() return NULL

/**
 * @brief A convenient macro to use inside of static array to define a
 * test definition which name is identical to the name of the test
 * function.
 *
 * Without @c ctDefineTest:
 *
 * @code{.c}
 * static ctTestDefinition tests[] = {
 *         {.name = "testPaletteCreate",
 *          .fn   = testPaletteCreate},
 *         {.name = "testPaletteDestroy",
 *          .fn   = testPaletteDestroy},
 * };
 * @endcode
 *
 * With @c ctDefineTest:
 *
 * @code{.c}
 * static ctTestDefinition tests[] = {
 *         ctDefineTest(testPaletteCreate),
 *         ctDefineTest(testPaletteDestroy),
 * };
 * @endcode
 */
#define ctDefineTest(testFn)                                                   \
	{                                                                      \
		.name = #testFn, .fn = testFn                                  \
	}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
CONKER_TEST_EXTERN_C_END
#endif
