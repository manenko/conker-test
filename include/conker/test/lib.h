#pragma once

/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             conker/test/lib.h
 * Created:          29-12-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus
#	define CONKER_TEST_EXTERN_C_BEGIN extern "C" {
#	define CONKER_TEST_EXTERN_C_END }
#else
#	define CONKER_TEST_EXTERN_C_BEGIN
#	define CONKER_TEST_EXTERN_C_END
#endif

#ifndef DOXYGEN_SHOULD_SKIP_THIS
CONKER_TEST_EXTERN_C_BEGIN
#endif
#include <conker/test/export.h>
#ifndef DOXYGEN_SHOULD_SKIP_THIS
CONKER_TEST_EXTERN_C_END
#endif
