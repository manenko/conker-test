/*
 * conker/test.c
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <conker/test.h>
#include <stdlib.h>
#include <string.h>

ctAssertionFailure *
ctCreateAssertionFailure(
	char const *message,
	char const *expression,
	char const *file,
	char const *function,
	size_t      line)
{
	ctAssertionFailure *failure = malloc(sizeof(ctAssertionFailure));

	failure->message   = message;
	failure->expession = expression;
	failure->file      = file;
	failure->function  = function;
	failure->line      = line;

	return failure;
}

void
ctDestroyAssertionFailure(ctAssertionFailure *failure)
{
	free(failure);
}

ctTestRunResult *
ctCreateTestRunResult(char const *name, ctAssertionFailure *failure)
{
	ctTestRunResult *result = malloc(sizeof(ctTestRunResult));

	result->name    = name;
	result->failure = failure;

	return result;
}

void
ctDestroyTestRunResult(ctTestRunResult *result)
{
	if (result == NULL) {
		return;
	}

	ctDestroyAssertionFailure(result->failure);
	free(result);
}

ctTestRunResults *
ctCreateTestRunResults(size_t count)
{
	ctTestRunResults *results = malloc(sizeof(ctTestRunResults));

	results->results = calloc(count, sizeof(ctTestRunResult *));
	results->count   = count;

	return results;
}

void
ctDestroyTestRunResults(ctTestRunResults *results)
{
	if (results == NULL) {
		return;
	}

	for (size_t i = 0; i < results->count; ++i) {
		free(results->results[i]);
	}

	free(results->results);
	free(results);
}

ctTestRunReport *
ctCreateTestRunReport(ctTestRunResults *results)
{
	ctTestRunReport *report = malloc(sizeof(ctTestRunReport));

	report->results              = results;
	report->successfulTestsCount = 0;
	report->failedTestsCount     = 0;

	for (size_t i = 0; i < results->count; ++i)
		if (results->results[i]->failure == NULL) {
			++report->successfulTestsCount;
		} else {
			++report->failedTestsCount;
		}

	report->successfulTests =
		calloc(report->successfulTestsCount, sizeof(ctTestRunResult *));

	report->failedTests =
		calloc(report->failedTestsCount, sizeof(ctTestRunResult *));

	size_t sIndex = 0;
	size_t fIndex = 0;

	for (size_t i = 0; i < results->count; ++i) {
		ctTestRunResult *result = results->results[i];

		if (result->failure == NULL) {
			report->successfulTests[sIndex++] = result;
		} else {
			report->failedTests[fIndex++] = result;
		}
	}

	return report;
}

void
ctDestroyTestRunReport(ctTestRunReport *report)
{
	if (report == NULL) {
		return;
	}

	free(report->successfulTests);
	free(report->failedTests);

	ctDestroyTestRunResults(report->results);

	free(report);
}

void
ctPrintTestRunResult(FILE *stream, ctTestRunResult *result)
{
	assert(stream != NULL);
	assert(result != NULL);

	if (result->failure == NULL) {
		fprintf(stream, "[SUCCESS] %s.", result->name);
	} else {
		fprintf(stream,
			"[FAILURE] %s. %s. [(%s) is false] at %s:%s:L%zu.",
			result->name,
			result->failure->message,
			result->failure->expession,
			result->failure->file, /* TODO: Strip file path! */
			result->failure->function,
			result->failure->line);
	}
}

void
ctPrintTestRunReport(FILE *stream, ctTestRunReport *report)
{
	assert(stream != NULL);
	assert(report != NULL);

	for (size_t i = 0; i < report->successfulTestsCount; ++i) {
		ctPrintTestRunResult(stream, report->successfulTests[i]);
		fprintf(stream, "\n");
	}

	for (size_t i = 0; i < report->failedTestsCount; ++i) {
		ctPrintTestRunResult(stream, report->failedTests[i]);
		fprintf(stream, "\n");
	}

	fprintf(stream,
		"\nTotal: %zu\nSucceeded: %zu\nFailed: %zu\n",
		report->results->count,
		report->successfulTestsCount,
		report->failedTestsCount);
}

ctTestRunResults *
ctRunTests(ctTestDefinition *tests, size_t count)
{
	assert(tests != NULL);
	assert(count != 0);

	ctTestRunResults *results = ctCreateTestRunResults(count);

	for (size_t i = 0; i < count; ++i) {
		ctTestRunResult *result =
			ctCreateTestRunResult(tests[i].name, tests[i].fn());
		results->results[i] = result;
	}

	return results;
}

ctTestDefinition *
ctFindTestDefinitionByName(
	ctTestDefinition *tests,
	size_t            testsCount,
	char *            testName)
{
	assert(testName != NULL);

	ctTestDefinition *notFound = NULL;

	if (tests == NULL || testsCount == 0) {
		return notFound;
	}

	for (size_t i = 0; i < testsCount; ++i) {
		ctTestDefinition *current = &tests[i];

		if (strcmp(current->name, testName) == 0) {
			return current;
		}
	}

	return notFound;
}

int
ctTestMain(
	int               argc,
	char **           argv,
	FILE *            stream,
	ctTestDefinition *tests,
	size_t            testsCount)
{
	if (argc > 2) {
		char *pathToProgram = argv[0];
		char *programName   = strrchr(pathToProgram, '/');

		if (programName == NULL) {
			programName = pathToProgram;
		} else {
			programName += 1;
		}

		fprintf(stream,
			"Usage: %s              run all tests\n"
			"   or: %s TEST_NAME    run test with the given name\n",
			programName,
			programName);
		return EXIT_FAILURE;
	}

	int exitCode = EXIT_SUCCESS;

	if (argc == 2) {
		char *            testName = argv[1];
		ctTestDefinition *test =
			ctFindTestDefinitionByName(tests, testsCount, testName);

		if (test == NULL) {
			fprintf(stderr,
				"[ERROR] Cannot find test '%s'.",
				testName);
			return EXIT_FAILURE;
		}

		ctTestRunResults *results = ctRunTests(test, 1);

		ctTestRunResult *result = results->results[0];
		ctPrintTestRunResult(stream, result);

		if (result->failure != NULL) {
			exitCode = EXIT_FAILURE;
		}

		ctDestroyTestRunResults(results);

		return exitCode;
	}

	ctTestRunResults *results = ctRunTests(tests, testsCount);

	ctTestRunReport *report = ctCreateTestRunReport(results);

	if (report->failedTestsCount > 0) {
		ctPrintTestRunReport(stream, report);
		exitCode = EXIT_FAILURE;
	}

	ctDestroyTestRunReport(report);

	return exitCode;
}
